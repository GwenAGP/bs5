# Prism

Bootstrap 5.3 upgraded theme with custom components build with Lit js.
Check the doc : https://theme.agp.fr 

## Installation

Clone the repo and update the dependencies with your needs in package.json. 
Here is a list of npm dependencies :

```bash
"dependencies": {
    "@popperjs/core": "^2.9.1",
    "bootstrap": "^5.3",
    "css-loader": "^6.7.3",
    "flatpickr": "^4.6",
    "gridjs": "^6.0",
    "highlight.js": "^11.7.0",
    "lit": "^2.6",
    "mini-css-extract-plugin": "^2.7.3",
    "postcss-loader": "^7.0.2",
    "sass": "^1.59.2",
    "sass-loader": "^13.2.0",
    "simple-datatables": "^7.2.0",
    "style-loader": "^3.3.1",
    "tom-select": "^2.2",
    "webpack": "^5.18.0"
}
```

Install dependencies 
```bash
yarn
```

Rename or remove the entry point from webpack.config.js and build the theme
```bash
yarn build
```

## Submodule integration

You can install this repository as a Submodule of an existing project :
```bash
git submodule add https://gitlab.com/GwenAGP/bs5.git
```

Submodule will download the current master and fix the last commit to your Git project
You can update your Submodule with 
```bash
git submodule update
```
Or you can update from the last remote master with flag ```--remote```
```bash
git submodule update --remote
```

## Webpack usage and alias

We recommend the usage of Webpack alias for calling the theme components in your js files as in the example below :

```bash
  resolve: {
    alias: {
      Prism: path.resolve(__dirname, 'src/'),
      Util: path.resolve(__dirname, 'src/js/util'),
      Components: path.resolve(__dirname, 'src/js/components'),
    }
  }
```

You can then import components in js using namespace :

```bash
import "Components/icon/obo-icon"
```

## Scss and Bootstrap 5.2

Here is a full list of what can be imported from the folder `scss` :

```bash
// 1. Include functions first (so you can manipulate colors, SVGs, calc, etc)
@import "bootstrap/scss/functions";

// 2. Include any default variable overrides here
@import "Prism/css/extends/variables.scss";
@import "Prism/css/components/variables.scss";

// 3. Include remainder of required Bootstrap stylesheets
@import "bootstrap/scss/variables";

// 4. Include any default map overrides here

// 5. Include remainder of required parts
@import "bootstrap/scss/maps"; 
@import "bootstrap/scss/mixins";
@import "bootstrap/scss/root";
@import "bootstrap/scss/reboot"; 

// 6. Optionally include any other parts as needed
@import "bootstrap/scss/transitions";
@import "bootstrap/scss/accordion";
@import "bootstrap/scss/alert";
@import "bootstrap/scss/badge";
@import "bootstrap/scss/breadcrumb";
@import "bootstrap/scss/button-group";
@import "bootstrap/scss/buttons";
@import "bootstrap/scss/card";
@import "bootstrap/scss/carousel";
@import "bootstrap/scss/close";
@import "bootstrap/scss/containers";
@import "bootstrap/scss/dropdown";
@import "bootstrap/scss/forms";
@import "bootstrap/scss/grid";
@import "bootstrap/scss/helpers";
@import "bootstrap/scss/images";
@import "bootstrap/scss/list-group";
@import "bootstrap/scss/modal"; // Requires transitions
@import "bootstrap/scss/nav";
@import "bootstrap/scss/navbar"; // Requires nav
@import "bootstrap/scss/offcanvas";
@import "bootstrap/scss/pagination";
@import "bootstrap/scss/placeholders";
@import "bootstrap/scss/popover";
@import "bootstrap/scss/progress";
@import "bootstrap/scss/spinners";
@import "bootstrap/scss/tables";
@import "bootstrap/scss/toasts";
@import "bootstrap/scss/tooltip";
@import "bootstrap/scss/type";
@import "bootstrap/scss/utilities";

// 7. Optionally include utilities API last to generate classes based on the Sass map in `_utilities.scss`
@import "bootstrap/scss/utilities/api";

// 8. Import override on some BS elements
@import "Prism/scss/extends/mixins/buttons";
@import "Prism/scss/extends/buttons"; // Requires buttons
@import "Prism/scss/extends/dropdown"; // Requires dropdown
@import "Prism/scss/extends/color-switch"; // Requires forms

// 9. Import custom element Sass
// May be removed or renamed in the future
@import "Prism/scss/custom/tom-select-theme"; 
@import "Prism/scss/custom/flatpickr";
@import "Prism/scss/custom/gridjs/index";
@import "Prism/scss/custom/drwr";

// 10. Icon font
@import "Prism/scss/sficons.scss";


```