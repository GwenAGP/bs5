const path = require('path');
const miniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = {
  mode: 'production',
  plugins: [new miniCssExtractPlugin()],
  entry: {
    app: './docs/app.js',
    oboButton: './docs/obo-button/script.js',
    oboConfirm: './docs/obo-confirm/script.js',
    oboNotistack: './docs/obo-notistack/script.js',
    oboModal: './docs/obo-modal/script.js',
    oboDrwr: './docs/drwr/script.js',
    oboPassword: './docs/obo-password/script.js',
    oboIcon: './docs/obo-icon/script.js',
    tomSelect: './docs/tom-select/script.js',
    flatPickr: './docs/flat-pickr/script.js',
    grid: './docs/grid/script.js',
    oboGrid: './docs/obo-grid/script.ts',
    oboDropdown: './docs/obo-dropdown/script.ts',
    
  },
  output: {
    filename: 'js/[name].js',
    path: path.resolve(__dirname, 'docs/dist'),
  },
  resolve: {
    extensions: ['.ts', '.js'],
    alias: {
      Util: path.resolve(__dirname, 'src/js/util'),
      Components: path.resolve(__dirname, 'src/js/components'),
    }
  },
  module: {
    rules: [
      {
        test: /\.(scss)$/,
        use: [
          {
            loader: miniCssExtractPlugin.loader
          },
          {
            loader: 'css-loader'
          },
          {
            loader: 'postcss-loader',
            options: {
              postcssOptions: {
                plugins: () => [
                  require('autoprefixer')
                ]
              }
            }
          },
          {
            loader: 'sass-loader'
          }
        ]
      },
      {
        test: /\.(ts)$/,
        exclude: [/node_modules/],
        use: {
          loader: 'ts-loader'
        }
      }
    ]
  }

};