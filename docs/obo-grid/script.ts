import { html } from 'lit';
import 'Components/prism-grid/prism-grid'

console.time('perf')

let columns = [
	{
		id: 'id',
		name: html`<u>ID</u>`,
		hidden: true
	},
	{
		id: 'code',
		name: 'Code',
		sort: true
	},
	{
		id: 'blacklist',
		name: 'Blacklist',
		sort: true,
		data: (cell: boolean ) => cell ? html`<span class='badge bg-danger'>Blacklisté</span>` : html`<span class='badge bg-success'>Autorisé</span>`
	},
	{
		id: 'custom',
		name: 'Custom',
		data: (cell: {Nom: string, Prenom: string}) => html`<b>${cell.Prenom} ${cell.Nom}</b>`
		
	},
	{
		id: 'updatedAt',
		name: 'updatedAt',
		data: (cell: Date) => new Date(cell).toLocaleString()
	}
]

document.addEventListener("DOMContentLoaded", () => { 
	const element = document.querySelector('prism-datatable')
	element!.columns = columns
	element!.onRowClick = (row: any) => {console.log('rowClick', row)}
	element!.onCellClick = (cell: any) => {console.log('cellClick', cell)}
})
