import 'Components/obo-modal.js'
import Drwr from 'Util/drwr.js'

document.getElementById('drwrdemo').addEventListener('click', e =>
{
  new Drwr({
    'content': '<h1>YOLO</h1><p>You only live once !</p><p class="mb-0">You can nest modal inside :</p><button class="btn btn-primary" id="drwrmodal">Fire modal</button>',
    'id': 'yolo-drwr',
    'removable': true,
    'width': '30%'
  }).show().addEventListener('closed', e => {console.log(e)})
  document.querySelector('#drwrmodal').addEventListener('click', (e) => {
		const modal = document.createElement('obo-modal')
		const header = document.createElement('h1')     
		const body = document.createElement('div')     
		const footer = document.createElement('div')
		header.classList.add('p-2', 'border-bottom', 'mb-0')   
		body.classList.add('p-2')   
		footer.classList.add('p-2', 'border-top')
		header.innerHTML = 'OBO Modal'   
		body.innerHTML = '<p>You can put what ever you want in it</p>'   
		footer.innerHTML = '<div class="btn btn-primary modal-close">Fermer</div>'   
		header.slot = 'header'
		body.slot = 'body'
		footer.slot = 'footer'
		modal.appendChild(header)
		modal.appendChild(body)
		modal.appendChild(footer)
		modal.show = true
		modal.fullscreen = false
		modal.removable = true
		document.querySelector('body').appendChild(modal)
	})
})

document.getElementById('drwrleft').addEventListener('click', e =>
{
  new Drwr({
    'content': '<h1>YOLO LEFT</h1><p>You only live once !</p>',
    'id': 'left-drwr',
    'position': 'left',
    'removable': true,
    'width': '30%'
  }).show()

})

document.getElementById('drwricons').addEventListener('click', e =>
{
  fetch('../').then( response => {
  	return response.text()
  }).then((data) => {
    new Drwr({
      'content': data,
      'id': 'big-drwr',
      'removable': true,
      'width': '50%'
    }).show()
  })
})