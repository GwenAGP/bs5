import TomSelect from 'tom-select'
import 'Components/obo-select.js'

///////////////
// TOMSELECT //
///////////////

if (document.getElementById("select-beast")){
	new TomSelect("#select-beast",{
		create: false,
		sortField: {
			field: "text",
			direction: "asc"
		},
		render:{
			'no_results':function(data,escape){
				return '<div class="no-results">No results found for "'+escape(data.input)+'", <a href="#">click here to add it.</a></div>';
			},
		}
	});
}

if (document.getElementById("select-multi")){
	new TomSelect("#select-multi",{
		maxItems: 3,
		plugins: ['dropdown_input', 'remove_button']
	});
}
