import { Grid, html, PluginPosition, BaseComponent, BaseProps, h} from "gridjs"
import { RowSelection } from "gridjs/plugins/selection";
import {frFR} from "Components/grid/l10n.js";
import Paginate from 'Components/grid/plugins/paginate.js';
import ToolBar from 'Components/grid/plugins/toolbar.js';
import SelectAction from 'Components/grid/plugins/selectAction.js';
import 'Components/icon/obo-icon.js'
import 'Components/grid/obo-grid.js'

let data = [
	{
		'id':1,
		'name': "Coca",
		'quantity': 128
	},
	{
		'id':2,
		'name': "Fanta",
		'quantity': 212
	},
	{
		'id':3,
		'name': "IceTea",
		'quantity': 327
	},
	{
		'id':4,
		'name': "Biere",
		'quantity': -32
	}
]

const grid = new Grid({ 
	columns: [
	{
		id:'id',
		name: '_id',
		hidden: true
	},
	{
    id: 'select',
    name: html(`
    	<div class="d-flex justify-content-center p-1">
    		<input class="form-check-input" type="checkbox" value="" id="check-all">
    	</div>
    `),
    sort: false,
    width: '32px',
    plugin: {
      // install the RowSelection plugin
      component: RowSelection,
      // RowSelection config
      props: {
        // use the "id" column as the row identifier
        id: (row) => row.cell(0).data
      }
    }
	},
	{
		id: 'name',
		name: 'Produit',
	},
	{
		id: 'quantity',
		name: 'Quantité en stock',
		formatter: (cell) => {
			if(cell > 0)
			{
				return html(`<span class="badge rounded-pill bg-success">${cell}</span>`)
			} 
			else 
			{
				return html(`<span class="badge rounded-pill bg-danger">${cell}</span>`)
			}
			
		}
	}
	],
	data: data,
	sort: true,
	pagination: {
		limit: 10,
		enabled: true,
		summary: true
	},
	search: true,
	language: frFR,
	className: {
    	table: 'table-hover'
  	}
});

grid.plugin.add({
	id: 'toolbar',
	component: ToolBar,
	position: PluginPosition.Header,
	order:1,
	toolbar: [
		{
		'class':'sf sf-table btn btn-icon-text btn-outline-dark me-2',
		'href':'/export/stock',
		'label':'Exporter',
		'content': h('obo-icon', {icon: 'table'}, '')
		},
		{
		'class':'btn btn-icon-text btn-primary',
		'href':'/transfer/new',
		'label':'Modifier le stock',
		'content': h('obo-icon', {icon: 'cart'}, '')
		},
	]
});

grid.plugin.add({
	id: 'emptyplugin',
	component: () => h('div', {}, ''),
	position: PluginPosition.Header,
	order:2
});

grid.plugin.add({
  id: 'paginate',
  component: Paginate,
  position: PluginPosition.Header,
  order:4
});	

grid.plugin.add({
  id: 'selectAction',
  component: SelectAction,
  position: PluginPosition.Footer,
  actions: [
  	{
  		'button': h('a', {className: "btn btn-danger ms-auto", onClick: (e) => deleting(e)}, 'Deleete'),
  		'fion': () => h('div', {}, '')
  	}
  ]
});

grid.plugin.get('search').order = 3
if(document.getElementById('stock-index-table'))
{
	grid.render(document.getElementById('stock-index-table'));	
}

