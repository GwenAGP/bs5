import 'Components/obo-notistack.js'
import Notifier from 'Util/notifier.js'

///////////////
// NOTISTACK //
///////////////
const notifier = new Notifier('end', 'top')
const notifierB = new Notifier('start', 'bottom')
var notifyList = document.querySelectorAll('.notify')
notifyList.forEach(el => {
    el.addEventListener("click", () => {
    	notifier.notify(el.dataset.ntType, el.dataset.ntLabel, el.dataset.ntAutoclose)
    })
})
var notifyListb = document.querySelectorAll('.notifyb')
notifyListb.forEach(el => {
    el.addEventListener("click", () => {
    	notifierB.notify(el.dataset.ntType, el.dataset.ntLabel, el.dataset.ntAutoclose)
    })
})