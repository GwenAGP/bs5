import 'Components/obo-modal.js'

document.querySelector('#firemodal').addEventListener('click', (e) => {
  const modal = document.createElement('obo-modal')
  const header = document.createElement('h1')     
  const body = document.createElement('div')     
  const footer = document.createElement('div')
  header.classList.add('p-2', 'border-bottom', 'mb-0')   
  body.classList.add('p-2')   
  footer.classList.add('p-2', 'border-top')
  header.innerHTML = 'OBO Modal'   
  body.innerHTML = '<p>You can put what ever you want in it</p>'   
  footer.innerHTML = '<div class="btn btn-primary modal-close">Fermer</div>'   
  header.slot = 'header'
  body.slot = 'body'
  footer.slot = 'footer'
  modal.appendChild(header)
  modal.appendChild(body)
  modal.appendChild(footer)
  modal.show = true
  modal.fullscreen = false
  modal.removable = true
  document.querySelector('body').appendChild(modal)
})
document.querySelector('#firemodal-fs').addEventListener('click', (e) => {
  const modal = document.createElement('obo-modal')
  const header = document.createElement('h1')     
  const body = document.createElement('div')     
  const footer = document.createElement('div')
  header.classList.add('p-2', 'border-bottom', 'mb-0')   
  body.classList.add('p-2')   
  footer.classList.add('p-2', 'border-top')
  header.innerHTML = 'OBO Modal'   
  body.innerHTML = '<p>You can put what ever you want in it</p>'   
  footer.innerHTML = '<div class="btn btn-primary modal-close">Fermer</div>'   
  header.slot = 'header'
  body.slot = 'body'
  footer.slot = 'footer'

  modal.appendChild(header)
  modal.appendChild(body)
  modal.appendChild(footer)
  modal.show = true
  modal.fullscreen = true
  modal.removable = true
  document.querySelector('body').appendChild(modal)
})
