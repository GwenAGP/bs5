import flatpickr from "flatpickr"
import "flatpickr/dist/l10n/fr.js"
import rangePlugin from 'flatpickr/dist/plugins/rangePlugin'

///////////////
// Flatpickr //
///////////////

flatpickr(".flaterpickerer", 
{
	monthSelectorType: 'static',
	closeOnSelect: false,
	onValueUpdate: (selectedDates, dateStr, instance) => 
	{
		instance.toggle()
	}
});

let lastUpDate
flatpickr(".flatpickr",
	{
		enableTime: false,
		locale: 'fr',
		dateFormat: "Y-m-d H:i",
		showMonths: 2,
		time_24hr: true,
		"plugins": [new rangePlugin({ input: secondInput})],
		onValueUpdate: (selectedDates, dateStr, instance) => {
            if (instance.selectedDates.length == 2 && lastUpDate){
            	instance.clear()
				var a = lastUpDate.getTime()
				var b = selectedDates[0].getTime()
				var c = selectedDates[1].getTime()
				if(a > b)
				{
					instance.setDate(selectedDates[0])
				} 
				else if (a == c)
				{
					instance.setDate(selectedDates[0])
				}
				else
				{
					instance.setDate(selectedDates[1])
				}
            	lastUpDate = false
            } else if (instance.selectedDates.length == 2) {
            	lastUpDate = instance.selectedDates[1];
            }
        },
	}
)