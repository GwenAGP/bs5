export default class Drwr {
  constructor(options)
  {
    this.content = options.content
    this.id = options.id ? options.id : 'drwr' 
    this.position = options.position === 'left' ? 'left' : 'right'
    this.removable = options.removable
    this.width = options.width ? options.width : '512px'
    this.render()
  }

  render()
  {
  	//Make the drwr element
  	this.drwr = document.createElement('div')
  	this.drwr.id = this.id
    this.drwr.classList.add('drwr', this.position)
    this.drwrcontent = document.createElement('div')
    this.drwrcontent.classList.add('drwr-content')
    this.drwrcontent.innerHTML = this.content
    this.drwrcontent.style.width = this.width
    this.drwr.appendChild(this.drwrcontent)
  	document.querySelector('body').appendChild(this.drwr)
    if(document.querySelector('#close-drwr'))
    {
      document.querySelector('#close-drwr').addEventListener('click', (e)=> this.hide())
    }
    this.drwr.addEventListener('click', (e) => {
      if(e.target.classList.contains('drwr'))
      {
        this.hide()
      }
    })
  }

  show(element) 
  {
    // Open Drwr with delay for programmatic instance
    setTimeout(()=>{this.drwr.classList.add('open')},100)
    // Set a new history
    history.pushState({},'','?'+this.id)
    // Listen to history change
    window.addEventListener("popstate", (e) => this._close(), { once: true })
    return this.drwr
  }

  hide() 
  {
    history.back()
  }

  _close()
  {
    window.removeEventListener("popstate", (e) => this.hide())
    this.drwr.dispatchEvent(new Event('hidden'))
    this.drwr.classList.remove('open')
    if(this.removable)
    {
      setTimeout(() => {this.drwr.remove()},300)
    }
  }
}