export default class Notifier {
	constructor(x = 'right', y = 'top', max = 3)
	{
		this.x = x === 'start' ? 'left' : x === 'end' ? 'right' : x
		this.y = y
		this.maxCount = max
		this.render()
	}

	render()
	{
		let els = document.getElementsByClassName(this.x + this.y)
		if(els.length > 0)
		{
			this.notifier = els[0]
		}
		else
		{
			this.notifier = document.createElement('obo-notifier');
			this.notifier.classList.add(this.x + this.y)
			this.notifier.style.position = 'fixed'
			this.notifier.style.zIndex = 1500
			this.notifier.style.transition = 'all 0.3s ease'
			this.notifier.style.margin = '32px'

			this.x == 'left' ? this.notifier.style.left = 0 : this.notifier.style.right = 0
			this.y == 'top' ? this.notifier.style.top = 0 : this.notifier.style.bottom = 0

			document.body.append(this.notifier)
		}
	}

	notify(type, message, autoclose)
	{
		this.checkCount()
		let obo = document.createElement('obo-notistack');
	  
	  	obo.type = type;
		obo.innerHTML = message;
		obo.autoclose = autoclose;

		this.notifier.append(obo);
	}
    
    checkCount(){
		for(let i = 0; i <= this.notifier.childNodes.length - this.maxCount; i++)
		{
			this.notifier.childNodes[i].fadeout()
		}
	}
}