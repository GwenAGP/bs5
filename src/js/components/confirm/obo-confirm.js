import { LitElement, html} from "lit";
import '../button/obo-button.js'

export default class oboConfirm extends LitElement {
  static get properties() {
    return {
      color: {type: String},
      confirmcolor: {type: String},
      icon: {type: Boolean},
      flags: {type: Array},
      path: {type: String},
      submit: {type: Boolean},
      timeout: {type: Number},
      clicked: {type: Boolean, attributes: false},
    }
  }

  constructor() {
    super()
    this.clicked = false
    this.submit = false
    this.timeout = 3000
  }

  render() {
    return html`
      <obo-button 
      ?icon="${this.icon}"
      color="${this._color}"
      @click="${this._handleClick}">
      <slot ?hidden=${this.clicked} name="idle"></slot>
      <slot ?hidden=${!this.clicked} name="confirm"></slot>
      </obo-button>
    `;
  }
  
  _handleClick(e){
    e.preventDefault()

    if(this.clicked)
    {
      this.dispatchEvent(this.event)
      if(this.submit)
      {
        this.closest('form').submit()
      }
      else if(this.path)
      {
        window.location.href = this.path
      }
    }
    else
    {
      clearTimeout(this.rollback)

      this._color = this.confirmcolor
      this.clicked = true
      this.rollback = setTimeout(()=>{
        this._color = this.color
        this.clicked = false
      }, this.timeout)
    }
  }

  connectedCallback() {
    super.connectedCallback()
    this._color = this.color
    this.event = new CustomEvent('obo-confirmed', {
      detail: {
        button: this,
        flags: this.flags
      }
    })
  }

  disconnectedCallback() {
    super.disconnectedCallback()
  }

}

customElements.define("obo-confirm", oboConfirm);

