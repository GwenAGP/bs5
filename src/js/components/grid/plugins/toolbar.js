import {h} from "gridjs"

export default function ToolBar(plugin) {
  	let bar = []
  	plugin.plugin.toolbar.forEach((e) => {

	  	bar.push(
	    	h(e.element ? e.element : 'a', 
	    	{
	            className: e.class,
	            id: e.id,
	            href: e.href,
	            onClick: e.action
	        }, 
	        e.content, e.label)
	  	)
  	})
  	
  	return h('div',{className: 'toolbar'}, bar)
}