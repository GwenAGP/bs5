import {html, h, useConfig, useTranslator} from "gridjs"
import { Fragment } from 'preact'

export default function Paginate()
{
  const config = useConfig()
  const instance = config.instance
  const pages = config.pages ? config.pages : [10,20,50,100]
  const pagination = config.pagination
  const position = this.props.plugin.position
  const _ = useTranslator()

  const setPagination = (n) => {
    if(n == 0)
    {
      instance.updateConfig({
        pagination: false,
      })
      instance.plugin.remove('pagination')
    }
    else
    {
      instance.updateConfig({
        pagination:{
          limit: n,
          enabled: true
        },
      })
    }
    instance.forceRender()
  }

  let options = []
  pages.forEach((n) => {
    if(pagination.limit == n)
    {
      options.push(h('option', {selected:true,value:n}, n))
    }
    else
    {
      options.push(h('option', {selected:false,value:n}, n))
    }
  })
  if(pagination.enabled)
  {
    options.push(h('option', {selected:false,value:0}, _('all')))
  }
  else
  {
    options.push(h('option', {selected:true,value:0}, _('all')))
  }

  return h('div', {className: 'paginate'}, h('div', {}, _('itemsPerPage')), h('select', {
    onChange: (e) => setPagination(e.target.value),
    className: 'form-select'
  }, h(Fragment, {}, options)))
}