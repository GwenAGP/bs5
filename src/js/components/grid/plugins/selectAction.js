import { h, html, useConfig, useEffect, useSelector, useTranslator, useStore} from "gridjs"
import Notifier from '../../../util/notifier'

export default function selectAction() 
{
  const _ = useTranslator()
  const gridnotifier = new Notifier()
  const config = useConfig()
	const instance = config.instance
  const state = useSelector(state => state)
  const store = useStore()
  const rowSelection = useSelector(state => state.rowSelection)
  const data = useSelector(state => state.data)

  // console.log(this.props.plugin.actions)

  const checkall = (e) =>
  {		
  	const chkbx = document.getElementsByClassName('gridjs-checkbox')
  	for (let i = 0; i < chkbx.length; i++)
  	{
  		if(e.currentTarget.checked)
  		{
			if (!chkbx[i].checked) {
	    		chkbx[i].click()
	  		}
  		}
  		else
  		{
			if (chkbx[i].checked) {
	    		chkbx[i].click()
	  		}
	  	}
		}
	}

  if(data)
  {
  	document.getElementById('check-all').addEventListener('click', checkall)
  	// Need this because forceRender doesn't update rowSelection
  	if(rowSelection)
  	{
	  	rowSelection.rowIds.forEach((id, k) => {
	  		if(!data.rows.find((row) => row._id == id))
	  		{
	  			rowSelection.rowIds.splice(k, 1)
	  		}
	  	})
  	}
  }
	
  const clear = (e) => {
  	let chkbx = document.getElementsByClassName('gridjs-checkbox')
  	for (let i = 0; i < chkbx.length; i++)
  	{
			if (chkbx[i].checked) {
    		chkbx[i].click()
	  	}
		}
		let checkallbtn = document.getElementById('check-all')
		if (checkallbtn.checked) {
			checkallbtn.click()
	  }
  }
  
  const deleting = (e) => {
		for(const row of rowSelection.rowIds) {
			let fil = data.rows.find((r) => r._id === row)
			let d = config.data.find((r) => r.id === fil.cell(0).data)
			let index = config.data.indexOf(d)
  		fetch('/api/product/'+fil.cell(0).data, {
				method: 'DELETE',
			}).then(function (response) {
				if (response.ok) {
					return response
				} else {
					return Promise.reject(response)			
				}
			}).then(data => {
				config.data.splice(index, 1)
			}).catch(err => {	
				gridnotifier.notify('danger',_('elementsDeleted'), 5000)
			})	
		}
		gridnotifier.notify('success',_('elementsDeleted'), 5000)
		instance.forceRender()
  }

  if(rowSelection && rowSelection.rowIds.length > 0)
	{
		return h('div', {className: "d-flex align-items-center grid-selected-box"},
			h('span', {className:"btn-close btn-close-white me-2", onClick: (e) => clear(e)}, ''),
			h('b', {}, rowSelection.rowIds.length, ' ', _('rowsSelected')),
			h('a', {className: "btn btn-danger ms-auto", onClick: (e) => deleting(e)}, 'Deleete')
		)
	}
}


  
// 	componentDidMount() {
// 	    this.instance.on('ready', () => {
// 	       // find the plugin with the give plugin ID
// 	       let checkboxPlugin = this.config.plugin.get('select');
// 	       document.getElementById('check-all').addEventListener('click', this.checkall)

// 	       // subscribe to the store events

// 	    });
// 	}

// 	componentDidUpdate()
// 	{
// 	  	let deletebutton = document.getElementById('batch-delete')
// 	  	if(deletebutton)
// 	  	{
// 	  		deletebutton.context = this
// 	  		deletebutton.addEventListener('click', this.delete)
// 	  		document.getElementById('clear-selection').addEventListener('click', this.clear)
// 	  	}
// 	}

// 	// Fire when wants to unclick all
//   clear()
// 	{	
//   	let chkbx = document.getElementsByClassName('gridjs-checkbox')
//   	for (let i = 0; i < chkbx.length; i++)
//   	{
// 			if (chkbx[i].checked) {
// 	    		chkbx[i].click()
// 	  		}
// 		}
// 		let checkall = document.getElementById('check-all')
// 		if (checkall.checked) {
// 			checkall.click()
// 	  	}
// 	}

//   	// Fire when wants to click all
//   checkall()
//   {		
//   	let chkbx = document.getElementsByClassName('gridjs-checkbox')
//   	for (let i = 0; i < chkbx.length; i++)
//   	{
//   		if(this.checked)
//   		{
// 			if (!chkbx[i].checked) {
// 	    		chkbx[i].click()
// 	  		}
//   		}
//   		else
//   		{
// 			if (chkbx[i].checked) {
// 	    		chkbx[i].click()
// 	  		}
// 	  	}
// 		}
// 	}

//   async delete(e)
//   {
//   	let rows = e.target.context.state.selectedRows
//   	let gridData = this.config.data
// 		for(const row of rows) {
// 	  		await fetch('/api/caterings/'+row, {
// 					method: 'DELETE',
// 				}).then(function (response) {
// 					if (response.ok) {
// 						return response
// 					} else {
// 						return Promise.reject(response)			
// 					}
// 				}).then((data) => {
// 					for( var i = 0; i < gridData.length; i++){ 
// 				        if ( gridData[i].id === row) { 
// 				            gridData.splice(i, 1); 
// 				        }
// 				    }
// 				}).catch(function (err) {	
// 					// notify('danger',`Impossible de supprimer l'element ${row}`)
// 				}
// 			)
// 		}
//   }
  
//   render() {
//     if (!this.state.selectedRows.length) {
//     	this.config.container.classList.remove('mb-4')
//       return h('b', {}, '');
//     }

//     this.config.container.classList.add('mb-4')

//     if(this.state.selectedRows.length == 1)
//     {
// 	    return html(`
// 	    	<div class="d-flex align-items-center grid-selected-box">
// 	      		<span id="clear-selection" class="btn-close btn-close-white me-2" aria-label="Close"></span>
// 	      		<b>${this.state.selectedRows.length} ${this._('rowSelected')}</b>
// 			    <a id="batch-delete" class="btn btn-danger ms-auto">Delete</a>
// 	      	</div>
// 	    `);
//     }

//     return html(`
//     	<div class="d-flex align-items-center grid-selected-box">
//       		<span id="clear-selection" class="btn-close btn-close-white me-2" aria-label="Close"></span>
//       		<b>${this.state.selectedRows.length} ${this._('rowsSelected')}</b>
// 		    <a id="batch-delete" class="btn btn-danger ms-auto">Delete</a>
//       	</div>
//     `);
//   }
// }