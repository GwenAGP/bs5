exports.enUS = {
  all: "Show all",
  itemsPerPage: "Items per page",
  rowsSelected: "rows selected",
  elementsDeleted: "All deleted",
  search: {
    placeholder: 'Type a keyword...',
  },
  sort: {
    sortAsc: 'Sort column ascending',
    sortDesc: 'Sort column descending',
  },
  pagination: {
    previous: 'Previous',
    next: 'Next',
    navigate: (page, pages) => `Page ${page} of ${pages}`,
    page: (page) => `Page ${page}`,
    showing: 'Showing',
    of: 'of',
    to: 'to',
    results: 'results',
  },
  loading: 'Loading...',
  noRecordsFound: 'No matching records found',
  error: 'An error happened while fetching the data',
},
exports.esES = {
    all: "Mostrar todo",
    itemsPerPage: "Resultados por página",
    rowsSelected: "filas seleccionadas",
    elementsDeleted: "All deleted",
    search: {
        placeholder: "🔍 Busqueda..."
    },
    sort: {
        sortAsc: "Ordenar la columna en orden ascendente",
        sortDesc: "Ordenar la columna en orden descendente"
    },
    pagination: {
        previous: "Anterior",
        next: "Siguiente",
        navigate: function(e, r) {
            return "Página " + e + " de " + r
        },
        page: function(e) {
            return "Página " + e
        },
        showing: "Mostrando los resultados",
        of: "sobre",
        to: "a",
        results: ""
    },
    loading: "Cargando...",
    noRecordsFound: "Nigún resultado encontrado",
    error: "Se produjo un error al recuperar datos"
}, 
exports.frFR = {
    all: "Afficher tout",
    itemsPerPage: "Résultats par page",
    rowsSelected: "lignes selectionnées",
    elementsDeleted: "Les elements ont bien été supprimés",
    search: {
        placeholder: "Recherche"
    },
    sort: {
        sortAsc: "Trier la colonne dans l'ordre croissant",
        sortDesc: "Trier la colonne dans l'ordre décroissant"
    },
    pagination: {
        previous: "<",
        next: ">",
        navigate: function(e, r) {
            return "Page " + e + " de " + r
        },
        page: function(e) {
            return "Page " + e
        },
        showing: "Affichage de",
        of: "sur",
        to: "à",
        results: 'résultats'
    },
    loading: "Chargement...",
    noRecordsFound: "Aucun résultat trouvé",
    error: "Une erreur est survenue lors de la récupération des données"
}, 
exports.itIT = {
    all: "Mostra tutto",
    itemsPerPage: "Risultati per pagina",
    rowsSelected: "righe selezionate",
    elementsDeleted: "Les elements ont bien été supprimés",
    search: {
        placeholder: "Ricerca..."
    },
    sort: {
        sortAsc: "Ordina ascendente",
        sortDesc: "Ordina discendente"
    },
    pagination: {
        previous: "Precedente",
        next: "Successivo",
        navigate: function(e, r) {
            return "Pagina " + e + " di " + r
        },
        page: function(e) {
            return "Pagina " + e
        },
        showing: "Mostra",
        of: "dei",
        to: "di",
        results: "risultati"
    },
    loading: "Caricamento...",
    noRecordsFound: "Nessun risultato trovato.",
    error: "Errore durante il caricamento dei dati."
}