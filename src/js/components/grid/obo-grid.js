import { Grid, html, PluginPosition, BaseComponent, BaseProps, h } from "gridjs"
import {frFR, enUS, esES, itIT} from "./l10n.js";
import Paginate from './plugins/paginate.js';
import ToolBar from './plugins/toolbar.js';

let oboptions =	{
	search: true,
	sort: true,
	resizable: true,
	pagination: {
		limit: 100,
		enabled: true,
	},
	language: enUS,
	className: {
    	table: 'table-hover'
  	}
};

class OboGrid extends HTMLElement {
	constructor() {
	    super();
	    
	    let viewpath = this.getAttribute('obo-viewpath')
	    let data = this.getAttribute('obo-data')
	    let columns = JSON.parse(this.getAttribute('obo-columns'))
	    let language = this.getAttribute('obo-language')
	    let toolbar = JSON.parse(this.getAttribute('obo-toolbar'))
	    let paginationlimit = JSON.parse(this.getAttribute('obo-pagination'))
	    let paginationfilter = JSON.parse(this.getAttribute('obo-page-filter'))

	    if(data)
	    {
	    	oboptions.data = JSON.parse(data)
	    }
	    else
	    {
	    	oboptions.data = 'No data'
	    }


	    if(columns)
	    {
	    	oboptions.columns = columns
		}

		if(language)
	    {
	    	switch (language) {
			  case 'frFR':
			    oboptions.language = frFR
			    break;
			  case 'enUS':
			    oboptions.language = enUS
			    break;
			  case 'itIT':
			    oboptions.language = itIT
			    break;
			  case 'esES':
			    oboptions.language = esES
			    break;
			  default:
				console.error('Grid component : Language unknown, fallback to enUS')			
			}
		}

		if(paginationlimit)
	    {
	    	oboptions.pagination.limit = paginationlimit
		}
		else
		{
			oboptions.pagination.enabled = false
		}

		if(viewpath)
	    {
	    	oboptions.viewpath = viewpath
		} 
		else 
		{
			console.warn('You need to configure a view path to have view on select')
		}
	 	const grid = new Grid(oboptions);

	 	if(toolbar)
	    {
	    	grid.plugin.add({
			  id: 'toolbar',
			  component: ToolBar,
			  position: PluginPosition.Header,
			  order:1,
			  toolbar: toolbar
			});	
		}

		if(paginationfilter)
	    {
	    	grid.plugin.add({
				id: 'emptyplugin',
				component: () => h('div', {}, ''),
				position: PluginPosition.Header,
				order:2
			});
	    	grid.plugin.add({
			  id: 'paginate',
			  component: Paginate,
			  position: PluginPosition.Header,
			  order:4
			});
			grid.plugin.get('search').order = 3
		}

	 	//Selectable Row
	 	if(viewpath){
			grid.on('rowClick', (...args) => {
				window.location.href = grid.config.viewpath+args[1].cells[0].data	
			});
		}

		grid.render(this)
	}
}

customElements.define('obo-grid', OboGrid);
