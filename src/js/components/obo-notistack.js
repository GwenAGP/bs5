import { LitElement, html, css} from "lit";

/**
 * Type [success, danger, warning, info, primary]
 * Autoclose Number - Time before autoclose in milliseconds
 */
export default class notistack extends LitElement {
  static get properties() {
    return {
      type: { type: String },
      autoclose: {type: Number},
    }
  }

  static styles = css`
    :host .alert{
      box-shadow:var(--obo-notistack-shadow, 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23));
      min-width:256px;
      max-width:384px;
      float:right;
      position: relative;
      padding: var(--obo-notistack-padding-y) var(--obo-notistack-padding-x);
      border: 1px solid transparent;
      border-radius: var(--obo-notistack-border-radius);
      margin-bottom:8px;
    }
    :host .alert ::slotted(a)
    {
      color:var(--obo-notistack-link-color, white);
      text-decoration:underline;
    }
    :host .alert ::slotted(a:hover)
    {
      color:var(--obo-notistack-link-hover-color, blue);
    }
    :host .alert-danger
    {
      color: white;
      background: var(--bs-danger, #f14160);
      border-color: var(--bs-danger, #f14160);
    }
    :host .alert-success
    {
      color: white;
      background: var(--bs-success, #00AA79);
      border-color: var(--bs-success, #00AA79);
    }
    :host .alert-warning
    {
      color: white;
      background: var(--bs-warning, #FFB646);
      border-color: var(--bs-warning, #FFB646);
    }
    :host .alert-info
    {
      color: white;
      background: var(--bs-info, #6450FF);
      border-color: var(--bs-info, #6450FF);
    }
    :host .alert-primary
    {
      color: white;
      background: var(--bs-primary, #30353A);
      border-color: var(--bs-primary, #30353A);
    }
    :host {   
      transition: right 0.2s ease, left 0.2s ease, opacity 0.4s ease, max-height 0.2s ease;
      max-height:256px;
      min-width:256px;
      max-width:384px;
      z-index:var(--obo-notistack-zindex, 1500);
      opacity:1;
      display:flex;
      position:relative;
    }
    :host .alert:hover{
      cursor:pointer;
    }
  `

  constructor() {
    super()
    this.autoclose = 0
  }

  render() {
    return html`
      <div class="alert alert-${this.type}" @click="${(e)=> this._handleClick(e)}"><slot></slot></div>
    `;
  }
  
  _handleClick(e){
    this.fadeout()
  }

  fadeout(){
    this.parentPosition == 'left' ? this.style.left = '-350px' : this.style.right = '-350px'
    this.style.opacity = '0'
    this.style.maxHeight = '0px'
    setTimeout(()=>{
      this.remove()
    },300)
  }

  connectedCallback() {
    super.connectedCallback()
    this.style.maxHeight = '0px'
    this.parentPosition = this.parentNode.style.right == '0px' ? 'right' : 'left'

    if(this.parentPosition == 'left')
    {
      this.style.left = '-350px' 

      setTimeout(()=>{
        this.style.left = '0px'
        this.style.maxHeight = '140px'
      },100)
    }
    else
    {
      this.style.right = '-350px' 

      setTimeout(()=>{
        this.style.right = '0px'
        this.style.maxHeight = '140px'
      },100)
    }
    // Set autoclose
    this.autoclose > 0 ? setTimeout(()=>{this.fadeout()}, this.autoclose) : false
  }
  disconnectedCallback() {
    super.disconnectedCallback()
    this.dispatchEvent(new Event('disconnected'))
  }
}

customElements.define("obo-notistack", notistack);

