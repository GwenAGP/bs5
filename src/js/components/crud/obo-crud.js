import { LitElement, html, css} from "lit";
import '../confirm/obo-confirm.js'
import Notifier from '../../util/notifier'
const notifier = new Notifier('right', 'top', 3)

export default class crud extends LitElement {
  static get properties() {
    return {
      entry: {type: String},
      list: {type: Array, attribute: false}
    }
  }

  static styles = css`
    .list
    {
      display:block;
    }
    .list .item
    {
      display:flex;
      justify-content: space-between; 
    }
  `

  constructor() {
    super()
    this.list = []
  }

  render() {
    console.log(this.list)
    let list = this.list.length != 0 ?
    html`<div class="skeletable">${this.list.map((el, k) => this.template(el, k))}</div>` :
    ''

    return html`
      <div class="form"><slot name="form" @submit=${this.post}></slot></div>
      <div class="list">${list}</div>
    `  
  }

  template(el, k)
  {
    return html`
      <div class="item">
        <div>
          <p class="">${el.title}</p>
        </div>
        <div class="buttons">
          <obo-confirm color="#FF4858" type="outline" confirmcolor="warning" icon @obo-confirmed=${()=>this.delete(el['@id'])}>
            <obo-icon slot="idle" icon="delete" size="sm"></obo-icon>
            <obo-icon slot="confirm" icon="danger" size="sm"></obo-icon>
          </obo-confirm>
        </div>
      </div>
    `
  }

  post(e)
  {
    e.preventDefault()
    const form = e.target
    fetch(this.entry, {
      method: form.method,
      body: JSON.stringify(Object.fromEntries(new FormData(form))),
      headers: {
        "Content-Type": "application/json"
      },
    }).then(function (response) {
      if (response.ok) {
        return response.json()
      } else {
        return Promise.reject(response)     
      }
    }).then((data) => {
      form.reset()
      this.get()
      notifier.notify('success', 'created', 3000)
    }).catch(function (err) {
      notifier.notify('danger', 'oups')
      console.warn("Data can't be loaded : ", err)
    });
  }

  get()
  {
    fetch(this.entry, {
      method: 'GET',
      headers: {
        "Content-Type": "application/json"
      },
    }).then(function (response) {
      if (response.ok) {
        return response.json()
      } else {
        return Promise.reject(response)     
      }
    }).then((data) => {
      this.list = data['hydra:member']
    }).catch(function (err) {
      notifier.notify('danger', 'Impossible de fetch GET')
      console.warn("Data can't be loaded : ", err)
    });
  }

  delete(path)
  {
    fetch(path, {
      method: 'DELETE',
      headers: {
        "Content-Type": "application/json"
      },
    }).then((response) => {
      if (response.ok) {
        notifier.notify('success', "Element supprimé", 5000)
        this.get()
      } else {
        notifier.notify('danger', "L'element ne peut pas être supprimé")
        console.warn("Document can't be deleted : ", err)    
      }
    })
  }

  connectedCallback()
  {
    super.connectedCallback();
    this.get()
  }
}

customElements.define("obo-crud", crud);
