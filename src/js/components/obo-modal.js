import { LitElement, html, css} from "lit";
import { classMap } from 'lit/directives/class-map.js';
import { styleMap } from 'lit/directives/style-map.js';

export default class modal extends LitElement {
  static get properties() {
    return {
      identifier: {type: String},
      show: {type: Boolean},
      width: {type: String},
      fullscreen: {type: Boolean},
      removable: {type: Boolean}
    }
  }

  static styles = css`
    :host .modal-container 
    {
      display:none;
      position:fixed;
      width:100%;
      top:0;
      left:0;
      height:100%;
      opacity:1;
      transition: opacity 0.2s ease;
    }
    :host .modal-container.show
    {
      display:block;
      z-index:var(--obo-modal-zindex, 2000);
      background-color:var(--obo-modal-backdrop-bg, rgba(0,0,0,0.3));
    }
    :host .obo-modal
    {
      margin:64px auto 0;
      box-sizing:border-box;
      height:auto;
      border: var(--obo-modal-border-width) solid var(--obo-modal-border-color);
      border-radius:var(--obo-modal-border-radius, 8px);
      background:var(--obo-modal-content-bg, white);
      padding:var(--obo-modal-padding);
      transform: translate(0, 0);
      transition: transform 0.2s ease;
    }
    :host .transi
    {
      opacity: 0;
    }
    :host .transi .obo-modal
    {
      transform: translate(0, 64px);
    }
    .modal-header, .modal-body, .modal-footer
    {
      padding:var(--obo-modal-body-padding);
    }
    :host .modal-container .obo-modal .modal-content
    {
      display:grid;
      grid-template-rows: min-content auto min-content;
    }
    :host .modal-container.fullscreen .obo-modal
    {
      margin:0;
      border-radius:0;
    }
    :host .modal-container.fullscreen .obo-modal .modal-content
    {
      height:100vh;
      overflow:scroll;
    }
  `;

  constructor() {
    super()
    this.identifier = "obo-modal"
    this.show = false
    this.width = '512px'
    this.fullscreen = false
    this.removable = false
  }

  render() {
    const styles = {
      width: this.fullscreen ? '100%' : this.width,
    }
    const classes = {show: this.show, fullscreen: this.fullscreen}
    return html`
    <div class="modal-container ${classMap(classes)}" @click="${this._click}">
      <div class="obo-modal" style=${styleMap(styles)} id="${this.identifier}">
          <div class="modal-content">
            <div class="modal-header">
              <slot name="header"></slot>
            </div>
            <div class="modal-body">
              <slot name="body"></slot>
            </div>
            <div class="modal-footer">
              <slot name="footer"></slot>
            </div>
          </div>
      </div>
    </div>
    `  
  }

  _show()
  {
    this.show = true
    this._transi()
  }
  _hide()
  {
    this.shadowRoot.querySelector(".modal-container").classList.toggle('transi')
    setTimeout(
      () => {
        this.show = false
        this.removable ? this.remove() : ''
      },
      210
    )

    this.dispatchEvent(new Event("hidden"))
  }
  _click(e)
  {
    if(e.target.classList.contains('modal-container'))
    {
      this._hide()
    }
  }
  _transi()
  {
    this.shadowRoot.querySelector(".modal-container").classList.toggle('transi')
    setTimeout(
      () => this.shadowRoot.querySelector(".modal-container").classList.toggle('transi'),
      10
    )
  }
  firstUpdated()
  {
    this._transi()
    this.shadowRoot.querySelectorAll("slot").forEach(slot => 
    {
      slot.assignedNodes({flatten: true}).forEach(el => {
        const closable = el.querySelectorAll('.modal-close')
        closable.forEach(el => el.addEventListener('click', (e) => this._hide()))
      })
    })
  }
}

customElements.define("obo-modal", modal);