import { LitElement, html, css} from "lit";
import './icon/obo-icon.js'

export default class oboPassword extends LitElement {
  static get properties() {
    return {
      type: { type: String, attributes: false },
      label: { type: String },
    }
  }

  static styles = css`
    :host
    {
      display:block;
    }
    :host .group
    {
      position:relative;
    }
    :host obo-icon
    {
      position:absolute;
      color:var(--obo-password-icon-color);
      padding:8px;
      top:calc(50% - 16px);
      right:8px;
    }
    obo-icon:hover
    {
      cursor:pointer;
      color:var(--obo-password-icon-hover-color);
    }
  `

  constructor() {
    super()
    this.type = 'password'
  }

  render() {

    this.icon = this.type === "password" ?
          html`<obo-icon size='sm' icon='eye' type='btn-icon' @click="${this._handleClick}"></obo-icon>`:
          html`<obo-icon size='sm' icon='eye_close' type='btn-icon' @click="${this._handleClick}"></obo-icon>`

    const label = this.label === 'undefined' ? '' : html`<label for="obo-password">${this.label}</label>`

    return html`
      ${label}
      <div class="group">
        <slot name="inputfield"></slot>
	      ${this.icon}
      </div>
    `

  }
  
  _handleClick(e){
    this.type = this.type === 'password' ? 'text' : 'password'
    const nodes = this.shadowRoot.querySelector('slot[name=inputfield]').assignedNodes({flatten: true})
    if(nodes.filter(node => node.matches('input')).length > 0)
    {
      nodes.filter(node => node.matches('input'))[0].type = this.type
      nodes.filter(node => node.matches('input'))[0].focus()
    }
    else
    {
      nodes.forEach(el => 
      {
        el.querySelector('input').type = this.type
        el.querySelector('input').focus()
      })
    }
  }
}

customElements.define("obo-password", oboPassword);

