import { css } from 'lit';
export const bsStyles = css`
    table {
      background: var(--bs-body-bg);
      border-collapse: collapse;
      border-color: var(--bs-border-color);
      border-style: solid;
      border-width:1px;
      width: 100%;
      text-align: left;
    }
    #wrapper.rounded
    {
    	border-radius: var(--bs-border-radius);
      border-color: var(--bs-border-color);
    	border-style: solid;
    	border-width: 1px;
    	overflow:hidden;
    	position:relative;
    }
    .rounded .table
    {
    	border-radius: var(--bs-border-radius);
    	border-width:0px;
    }
    .rounded .tr
    {
    	border:0;
    }
    thead
    {
    	background-color: var(--bs-gray-100)
    }
    .table>:not(caption)>*>* {
	    padding: 12px 24px;
	    background-color: var(--bs-table-bg);
	    border-width: 0 0 1px;
	    box-shadow: inset 0 0 0 9999px var(--bs-table-accent-bg);
	}
	.table-bordered>:not(caption)>* {
        border-color: var(--bs-border-color);
    	border-style: solid;
    	border-width: 0 0 1px;
	}
	.table-footer
	{
		padding: 12px 24px;
	}
	.sort
	{
		float: right;
		height: 24px;
		width: 13px;
		background-color: transparent;
		background-repeat: no-repeat;
		background-position: center center;
		background-size:16px;
		background-image: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIzMiIgaGVpZ2h0PSIzMiIgZmlsbD0iIzI3MjUyNyIgdmlld0JveD0iMCAwIDI1NiAyNTYiPjxwYXRoIGQ9Ik0xODQuNDksMTY3LjUxYTEyLDEyLDAsMCwxLDAsMTdsLTQ4LDQ4YTEyLDEyLDAsMCwxLTE3LDBsLTQ4LTQ4YTEyLDEyLDAsMCwxLDE3LTE3TDEyOCwyMDdsMzkuNTEtMzkuNTJBMTIsMTIsMCwwLDEsMTg0LjQ5LDE2Ny41MVptLTk2LTc5TDEyOCw0OWwzOS41MSwzOS41MmExMiwxMiwwLDAsMCwxNy0xN2wtNDgtNDhhMTIsMTIsMCwwLDAtMTcsMGwtNDgsNDhhMTIsMTIsMCwwLDAsMTcsMTdaIj48L3BhdGg+PC9zdmc+");
	}
	.sortable:hover
	{
		cursor:pointer;
		background-color: var(--bs-light)
	}
	.up
	{
		background-image: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIzMiIgaGVpZ2h0PSIzMiIgZmlsbD0iIzI3MjUyNyIgdmlld0JveD0iMCAwIDI1NiAyNTYiPjxwYXRoIGQ9Ik0yMDguNDksMTIwLjQ5YTEyLDEyLDAsMCwxLTE3LDBMMTQwLDY5VjIxNmExMiwxMiwwLDAsMS0yNCwwVjY5TDY0LjQ5LDEyMC40OWExMiwxMiwwLDAsMS0xNy0xN2w3Mi03MmExMiwxMiwwLDAsMSwxNywwbDcyLDcyQTEyLDEyLDAsMCwxLDIwOC40OSwxMjAuNDlaIj48L3BhdGg+PC9zdmc+");
	}
	.down
	{
		background-image: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIzMiIgaGVpZ2h0PSIzMiIgZmlsbD0iIzI3MjUyNyIgdmlld0JveD0iMCAwIDI1NiAyNTYiPjxwYXRoIGQ9Ik0yMDguNDksMTUyLjQ5bC03Miw3MmExMiwxMiwwLDAsMS0xNywwbC03Mi03MmExMiwxMiwwLDAsMSwxNy0xN0wxMTYsMTg3VjQwYTEyLDEyLDAsMCwxLDI0LDBWMTg3bDUxLjUxLTUxLjUyYTEyLDEyLDAsMCwxLDE3LDE3WiI+PC9wYXRoPjwvc3ZnPg==")
	}

	#loadbar
	{
		display: inline-block;
		height: 100%;
		width:100%;
		position: absolute;
		overflow: hidden;
		background-color: var(--bs-light);
		opacity:0.3;
		z-index:1000;
	}
	#loadbar::after {
		position: absolute;
		top: 0;
		right: 0;
		bottom: 0;
		left: 0;
		transform: translateX(-100%);
  		background-image: linear-gradient(90deg, rgba(100, 100, 100, 0) 0, rgba(100, 100, 100, 0.2) 20%, rgba(100, 100, 100, 0.5) 60%, rgba(100, 100, 100, 0));
		animation: shimmer 2s infinite;
		content: '';
	}

	@keyframes shimmer {
		100% {
		  transform: translateX(100%);
		}
	}
}
`
