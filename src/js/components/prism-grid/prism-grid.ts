import { LitElement, html, render } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import '../pagination/prism-pagination'
import '../dropdown/obo-dropdown'
import '../button/obo-button'
import '../icon/obo-icon'
import { bsStyles } from './grid.css'

type Conf = {
  limit: number;
};

type Col = {
  id:string; name:string|object; hidden?:boolean; sort?:boolean, data?:Function
}

interface Hydra<T> {
  '@context': string;
  '@id': string;
  '@type': string;
  'hydra:totalItems': number;
  'hydra:member': T[];
}

@customElement('prism-datatable')
export class PrismGrid extends LitElement {  
  @property({
    converter: (attrValue: string | null) => {
      if (attrValue)
        return JSON.parse(attrValue);
      else
        return undefined;
    }
  })
  columns: Array<Col> = []

  @property({type: Object})
  conf: Conf = {
    limit: 10
  }

  @property({type: String})
  endpoint: string = '';

  @property({type: Number})
  page: number = 1;

  @property({type: Number})
  lastpage: number = 100;

  @property({type: Number})
  totalItems: number = 0;

  @property({type: Object})
  sorted: {[key:string]: any} = {};

  @property({type: Function})
  onRowClick: Function = () => false

  @property({type: Function})
  onCellClick: Function = () => false

  @property({
    converter: (attrValue: string | null) => {
      if (attrValue)
        return JSON.parse(attrValue);
      else
        return undefined;
    }
  })
  data: Hydra<Array<Object>> = {
    '@context': '',
    '@id': '',
    '@type': '',
    'hydra:totalItems': 0,
    'hydra:member': []
  }
  
  @state()
  protected loading = true;

  static override styles = bsStyles
  
  override render() {
    let i = 0
    return html`
    <div id="toolbox">
      <obo-dropdown>
        <obo-button slot="button" color="primary" type="outline" icon>
          <obo-icon icon="ellipsis-v"></obo-icon>
        </obo-button>
        <div slot="content">
          ${this.columns.map((col) => 
              html`<li>    
                <input type="checkbox" id="${col.id}" ?checked=${!col.hidden} @click=${(_e: PointerEvent) => this._updateColumns(_e, col)}/>
                <label for="${col.id}">${col.name}</label>
                </li>`
          )}
        </div>
      </obo-dropdown>
    </div>
    <div id="wrapper" class="rounded">
    ${this.loading ? html`<div id="loadbar"></div>` : ''}
    <table class="table table-bordered" ${this.loading ? 'loading' : 'ready'}">
      <thead class="table-light">
        <tr>
          ${this.columns.map((col) => 
          {
            if(col.hidden)
            {
              return
            }
            if(col.sort)
            {
              let sorticon
              if(this.sorted[col.id] === 'ASC')
              {
                sorticon = html`<span class="sort up"></span>` 
              }
              else if(this.sorted[col.id] === 'DESC')
              {
                sorticon = html`<span class="sort down"></span>`
              }
              else
              {
                sorticon = html`<span class="sort"></span>`  
              }
              return html`<th class="sortable" @click=${(_e: PointerEvent) => this.sort(_e, col.id)}>${sorticon}${col.name}</th>` 
            }
            else
            {
              return html`<th>${col.name}</th>`  
            }
          })}
        </tr>
      </thead>
      <tbody>
      ${this.data['hydra:member'].map((member:object) =>
        { 
          const cells: Array<Object> = []
          this.columns.map((col) => 
          {
            if(col.hidden)
            {
              return
            }
            const cell:any = member[col.id as keyof typeof member]
            if(col.data)
            {
              cells.push(html`<td @click=${(_e:PointerEvent) => this._cellClickHandler(_e, cell)}><slot name="cell-${i}"></slot></td>`)
            }
            else
            {
              cells.push(html`<td @click=${(_e:PointerEvent) => this._cellClickHandler(_e, cell)}>${cell}</td>`)
            }
            i++
          })
          return html`<tr @click=${(_e:PointerEvent) => this._rowClickHandler(_e, member)}>${cells}</tr>`
        }
      )}
      </tbody>
    </table>
    <div class="table-footer">
    <prism-pagination @setPage=${(_e: CustomEvent) => this._setPage(_e)} shown=3 position=${this.page} length=${this.lastpage}></prism-pagination>
    </div>
    </div>
    <p></p>
    ${this._renderFormatedCells()}
    `
  }

  override updated()
  {
    console.timeLog('perf')
  }

  _renderFormatedCells()
  {
    let i = 0 
    const rows: Array<Object> = []
    this.data['hydra:member'].map((member:object) =>
    {
      const cells: Array<Object> = []
      this.columns.map((col) => 
      {
        if(col.hidden)
        {
          return
        }
        const cell:any = member[col.id as keyof typeof member]
        if(col.data)
        {
          cells.push(html`<span slot="cell-${i}">${col.data(cell, member)}</span>`)
        }
        i++
      })
      rows.push(cells)
    })
    render(rows, this)
  }

  override connectedCallback() {
    super.connectedCallback()
    this.request()
  }

  _setPage(e: CustomEvent)
  {
    this.page = e.detail
    this.loading = true
    this.request()
    console.log('change page', e)
  }

  request()
  {
    var url = new URL(this.endpoint);
    url.searchParams.append('page', this.page.toString());
    for(const [key, value] of Object.entries(this.sorted))
    {
      url.searchParams.append(`order[${key}]`, value);
    }
    
    fetch(url, {
      headers: {
        "Content-Type": "application/ld+json"
      }
    }).then(response => {
      console.log(response)
      if (response.ok) {
        return response.json()
      } else {
        console.log('fail to fetch')
        return Promise.reject(response)
      }
    }).then(data => 
    {
      console.log(data)
      var lastpage = new URLSearchParams(data['hydra:view']['hydra:last'])
      this.lastpage = Number(lastpage.get('page'))
      this.data = data
      this.totalItems = data['hydra:totalItems']
      this.loading = false
    })
  }

  sort(e: PointerEvent, id: string)
  {
    this.loading = true
    this.page = 1
    //Reset sorted if shift key is pressed
    if(!e.shiftKey)
    {
      this.sorted = {[id]: this.sorted[id]}
    }
    if(this.sorted[id] && this.sorted[id] === 'ASC')
    {
      this.sorted = {...this.sorted, [id]: 'DESC'}
    }
    else if(this.sorted[id] && this.sorted[id] === 'DESC')
    {
      this.sorted = {...this.sorted, [id]: 'ASC'}
    }
    else
    {
      this.sorted = {...this.sorted, [id]: 'ASC'}
    }
    this.request()
  }

  _rowClickHandler(_e: PointerEvent, member: any)
  {
    this.onRowClick(member)
  }
  _cellClickHandler(_e: PointerEvent, cell: any)
  {
    this.onCellClick(cell)
  }

  _updateColumns(_e: PointerEvent, col: Col)
  {
    this.columns[this.columns.indexOf(col)].hidden = !col.hidden
    this.columns = [...this.columns]
  }
}

declare global {
  interface HTMLElementTagNameMap {
    "prism-datatable": PrismGrid;
  }
}