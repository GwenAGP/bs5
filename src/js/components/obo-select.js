import { LitElement, html, css} from "lit";
import {map} from 'lit/directives/map.js';
import TomSelect from 'tom-select'

export default class oboSelect extends LitElement {
	static get properties() {
		return {
		  	maxitems: { type: Number},
		  	placeholder: { type: String},
		  	name: { type: String},
		  	required: {type: Boolean},
		  	options: {attribute: false}
		}
	}

  	constructor() {
		super()
		this.maxitems = 4;
		this.required = false;
    	this.placeholder = 'World';
    	this.name = 'obo-select';
    	this.options = [];
		this.querySelectorAll('option').forEach(opt => this.options.push(opt.cloneNode(true)))
		this.innerHTML = ''	
	}

  	render() {
  		return html`
  			<select id="selectize" class="form-select" ?required=${this.required == true} name=${this.name}>
  				${this.options}
  			</select>
  		`
	}

	createRenderRoot() {
		return this;
	}
  
  	connectedCallback() {
    	super.connectedCallback();
	}

	firstUpdated() {
		super.firstUpdated();
		new TomSelect(this.renderRoot.querySelector('#selectize'),{
			maxItems: this.maxitems,
			placeholder: this.placeholder,
			plugins: ['dropdown_input', 'remove_button']
		});
	}

	disconnectedCallback() {
		super.disconnectedCallback();
	}

}

customElements.define("obo-select", oboSelect);


