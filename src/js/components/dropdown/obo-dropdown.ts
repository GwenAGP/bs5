import { LitElement, html, css } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import { styleMap } from 'lit/directives/style-map.js';
import { createPopper } from '@popperjs/core';
import type { Instance, Placement } from '@popperjs/core';

@customElement('obo-dropdown')
export class OboDropdown extends LitElement {
  @property({type: String})
  placement:Placement = 'bottom-start';

  @property({type: String})
  maxheight = 'auto';

  @property({type: String})
  maxwidth = 'auto';

  @state()
  protected open = false;

  protected popper?: Instance

  protected callback: EventListener = (e: Event) => this.outsideClickHandler(e)

  static override styles = css`
    :host
    {      
      display:inline-block;
    }
    .closed
    {
      display:none
    }
    .open
    {
      display:block;
    }
    #button
    {
      position:relative;
    }
    slot[name="content"]::slotted(*)
    {
      min-width: var(--obo-dropdown-min-width);
      padding: var(--obo-dropdown-padding-y) var(--obo-dropdown-padding-x);
      margin: 0;
      list-style: none;
      font-size: var(--obo-dropdown-font-size);
      color: var(--obo-dropdown-color);
      text-align: left;
    }
    slot[name="content"]
    {
      width:auto;
      position:absolute;
      background: white;
      z-index: var(--obo-dropdown-zindex);
      background-color: var(--obo-dropdown-bg);
      background-clip: padding-box;
      border: var(--obo-dropdown-border-width) solid var(--obo-dropdown-border-color);
      border-radius: var(--obo-dropdown-border-radius);
      box-shadow: var(--obo-dropdown-box-shadow);
    }
    slot[name="content"] li
    {
      padding: var(--obo-dropdown-item-padding-y) var(--obo-dropdown-item-padding-x);
    }
  `

  override firstUpdated()
  {
    console.log('firstUpdated')
    const content = this.shadowRoot!.querySelector('slot[name="content"')
    this.popper = createPopper(this, content as HTMLElement, {
      placement: this.placement,
      modifiers: [
      {
        name: 'offset',
        options: {
          offset: [0, 2],
        },
      },
    ],
    });
  }

  override render()
  {
    const styles = {
      'height': this.maxheight, 
      'overflow-y': this.maxheight ? 'scroll' : 'inherit',
      'width': this.maxwidth
    };
    return html`
      <div>
        <slot name="button" id="button" class="${this.open ? 'open' : ''}" @click=${this.openState}></slot>
        <slot name="content" id="list" style=${styleMap(styles)} class="${this.open ? 'open' : 'closed'}"></slot> 
      </div>
    `
  }

  outsideClickHandler(e: Event) {
    if(!(e.composedPath() as Array<HTMLElement>).includes(this))
    {
      this.closeState()
    }
  }

  openState()
  {
    this.open = !this.open
    this.popper!.update()

    //Handle close when clicked outside
    if(this.open)
    {
      document.addEventListener('click', this.callback, true)
    }
    else
    {
      document.removeEventListener('click', this.callback, true)
    }
  }

  closeState()
  {
    this.open = false
    document.removeEventListener('click', this.callback, true)
  }

}

declare global {
  interface HTMLElementTagNameMap {
    "obo-dropdown": OboDropdown;
  }
}