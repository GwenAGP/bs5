import { LitElement, html, css} from "lit";
import { styleMap } from 'lit/directives/style-map.js';

export default class oboButton extends LitElement {
  static get properties() {
    return {
      color: { type: String },
      type: { type: String },
      icon: {type: Boolean}
    }
  }
  static styles = css`
    :host {
      display:inline-block;
    }
    button {
      display: inline-block;
      vertical-align: middle;
      font-family: var(--bs-body-font-family);
      font-size: var(--bs-body-font-size);
      transition: var(--obo-btn-active-scale-transition);
      background:black;
      color:white;
      width:100%;
      padding: var(--obo-btn-padding-y) var(--obo-btn-padding-x);
      border-radius: var(--obo-btn-border-radius);
      border: var(--obo-btn-border-width) solid var(--obo-btn-border-color);
      line-height:var(--obo-btn-line-height);
    }
    button:active
    {
      transform: scale(var(--obo-btn-active-scale));
    }
    :host button:hover {
      cursor:pointer;
    }
    :host .btn-icon
    {
      padding: var(--obo-btn-padding-y);
      line-height: 1;
    }
    .primary
    {
      background: var(--bs-primary, blue);
    }
    .primary:hover
    {
      background: color-mix(in srgb, var(--bs-primary) 90%, black)
    }
    .secondary
    {
      background: var(--bs-secondary, blue);
    }
    .secondary
    {
      background: color-mix(in srgb, var(--bs-secondary) 90%, black)
    }
    .danger
    {
      background: var(--bs-danger, blue);
    }
    .danger:hover:not(.outline)
    {
      background: color-mix(in srgb, var(--bs-danger) 90%, black)
    }
    .warning
    {
      background: var(--bs-warning, blue);
    }
    .warning:hover
    {
      background: color-mix(in srgb, var(--bs-warning) 90%, black)
    }
    .success
    {
      background: var(--bs-success, blue);
    }
    .success:hover:not(.outline)
    {
      background: color-mix(in srgb, var(--bs-success) 90%, black)
    }
    .disabled
    {
      background: var(--bs-light, gray);
      color:var(--bs-gray, gray);
    }
    :host .disabled:hover {
      cursor: default;
      opacity: 0.65;
    }
    .outline:not(:hover)
    {
      background:transparent;
    }
    .outline:hover
    {
      color: white !important;
    }
    .outline.primary
    {
      border-color: var(--bs-primary, blue);
      color: var(--bs-primary, blue);
    }
    .outline.secondary
    {
      border-color: var(--bs-secondary, blue);
      color: var(--bs-secondary, blue);
    }
    .outline.danger
    {
      border-color: var(--bs-danger, red);
      color: var(--bs-danger, red);
    }
    .outline.warning
    {
      border-color: var(--bs-warning, yellow);
      color: var(--bs-warning, yellow);
    }
    .outline.success
    {
      border-color: var(--bs-success, green);
      color: var(--bs-success, green);
    }
  `;

  constructor() {
    super()
    this.icon = false
  }

  render() {
    let styles = {}
    if(this.color.charAt(0) === '#' && this.color.length === 7)
    {
      styles = {
        'background': this.color, 
      };
    }

    this.icon ? this.type += " btn-icon" : ''

    return html`
      <button type="button" class="${this.color} ${this.type}" @mouseover="${this.onHover}" @mouseout="${this.onHoverOut}" style=${styleMap(styles)}>
        <slot></slot>
      </button>
    `;
  }

  onHover(e)
  {
    this.shadowRoot.querySelector('button').style.background = `color-mix(in srgb, ${this.color} 90%, black)`
  }
  onHoverOut(e)
  {
    this.shadowRoot.querySelector('button').style.background = this.color
  }
}

customElements.define("obo-button", oboButton);
