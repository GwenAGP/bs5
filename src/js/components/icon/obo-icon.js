import { LitElement, html, css} from "lit";
import { iconsStyles } from './style.js';

export default class oboIcon extends LitElement {
  static get properties() {
    return {
      icon: {type: String},
      size: {type: String},
    }
  }

  static styles = [
    iconsStyles,
    css`
      :host, :host span
      {
        line-height: 1;
      }
    `
  ]

  constructor() {
    super()
    this.size = 'md'
  }

  render() {
    return html`<span class="sf-${this.size} sf-${this.icon}"></span>`
  }
}

customElements.get('obo-icon') || customElements.define("obo-icon", oboIcon);

