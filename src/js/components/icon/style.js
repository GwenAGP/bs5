import {css} from "lit";

export const iconsStyles = css`

[class^="sf-"]:before, [class*=" sf-"]:before {
   font-family: "sf-icons";
   font-style: normal;
   font-weight: normal;
   speak: none;
   display: inline-block;
   text-decoration: inherit;
   text-align: center;
  /* opacity: .8;
   */
  /* For safety - reset parent styles, that can break glyph codes*/
   font-variant: normal;
   text-transform: none;
  /* fix buttons height, for twitter bootstrap */
   line-height: 1em;
  /* Font smoothing. That was taken from TWBS */
   -webkit-font-smoothing: antialiased;
   -moz-osx-font-smoothing: grayscale;
  /* Uncomment for 3D effect */
  /* text-shadow: 1px 1px 1px rgba(127, 127, 127, 0.3);
   */
}
 .sf-lg:before {
   font-size: 32px;
   color: inherit;
}
 .sf-lg:before:hover {
   text-decoration: none;
}
 .sf:before, .sf-md:before {
   font-size: 24px;
   color: inherit;
}
 .sf:before:hover, .sf-md:before:hover {
   text-decoration: none;
}
 .sf-sm:before {
   font-size: 16px;
   color: inherit;
}
 .sf-sm:before:hover {
   text-decoration: none;
}
 .sf-clouds-flash-alt:before {
   content: '\ue800';
}
/* '' */
 .sf-sunrise:before {
   content: '\ue801';
}
/* '' */
 .sf-sun:before {
   content: '\ue802';
}
/* '' */
 .sf-moon:before {
   content: '\ue803';
}
/* '' */
 .sf-mist:before {
   content: '\ue804';
}
/* '' */
 .sf-wind:before {
   content: '\ue805';
}
/* '' */
 .sf-windy-rain:before {
   content: '\ue806';
}
/* '' */
 .sf-snow:before {
   content: '\ue807';
}
/* '' */
 .sf-snow-alt:before {
   content: '\ue808';
}
/* '' */
 .sf-snow-heavy:before {
   content: '\ue809';
}
/* '' */
 .sf-hail:before {
   content: '\ue80a';
}
/* '' */
 .sf-clouds:before {
   content: '\ue80b';
}
/* '' */
 .sf-clouds-flash:before {
   content: '\ue80c';
}
/* '' */
 .sf-cloud-sun:before {
   content: '\ue80d';
}
/* '' */
 .sf-cloud-moon:before {
   content: '\ue80e';
}
/* '' */
 .sf-fog-sun:before {
   content: '\ue80f';
}
/* '' */
 .sf-fog-moon:before {
   content: '\ue810';
}
/* '' */
 .sf-fog-cloud:before {
   content: '\ue811';
}
/* '' */
 .sf-fog:before {
   content: '\ue812';
}
/* '' */
 .sf-cloud:before {
   content: '\ue813';
}
/* '' */
 .sf-cloud-flash:before {
   content: '\ue814';
}
/* '' */
 .sf-cloud-flash-alt:before {
   content: '\ue815';
}
/* '' */
 .sf-drizzle:before {
   content: '\ue816';
}
/* '' */
 .sf-rain:before {
   content: '\ue817';
}
/* '' */
 .sf-windy:before {
   content: '\ue818';
}
/* '' */
 .sf-activity:before {
   content: '\ue819';
}
/* '' */
 .sf-add_alarm:before {
   content: '\ue81a';
}
/* '' */
 .sf-add:before {
   content: '\ue81b';
}
/* '' */
 .sf-alarm_success:before {
   content: '\ue81c';
}
/* '' */
 .sf-alarm:before {
   content: '\ue81d';
}
/* '' */
 .sf-alert_octagon:before {
   content: '\ue81e';
}
/* '' */
 .sf-alert_square:before {
   content: '\ue81f';
}
/* '' */
 .sf-alert:before {
   content: '\ue820';
}
/* '' */
 .sf-ancor:before {
   content: '\ue821';
}
/* '' */
 .sf-app_alt:before {
   content: '\ue822';
}
/* '' */
 .sf-app:before {
   content: '\ue823';
}
/* '' */
 .sf-area_line:before {
   content: '\ue824';
}
/* '' */
 .sf-area:before {
   content: '\ue825';
}
/* '' */
 .sf-attach:before {
   content: '\ue826';
}
/* '' */
 .sf-bag:before {
   content: '\ue827';
}
/* '' */
 .sf-bar_alt:before {
   content: '\ue828';
}
/* '' */
 .sf-bar:before {
   content: '\ue829';
}
/* '' */
 .sf-basket:before {
   content: '\ue82a';
}
/* '' */
 .sf-bed:before {
   content: '\ue82b';
}
/* '' */
 .sf-bell_alarm:before {
   content: '\ue82c';
}
/* '' */
 .sf-bell_cross:before {
   content: '\ue82d';
}
/* '' */
 .sf-bell_ringing_alt:before {
   content: '\ue82e';
}
/* '' */
 .sf-bell_ringing:before {
   content: '\ue82f';
}
/* '' */
 .sf-bell:before {
   content: '\ue830';
}
/* '' */
 .sf-block:before {
   content: '\ue831';
}
/* '' */
 .sf-bluetooth:before {
   content: '\ue832';
}
/* '' */
 .sf-bold:before {
   content: '\ue833';
}
/* '' */
 .sf-bookmark:before {
   content: '\ue834';
}
/* '' */
 .sf-bottom_left:before {
   content: '\ue835';
}
/* '' */
 .sf-bottom_right:before {
   content: '\ue836';
}
/* '' */
 .sf-box:before {
   content: '\ue837';
}
/* '' */
 .sf-broadcasting:before {
   content: '\ue838';
}
/* '' */
 .sf-browser:before {
   content: '\ue839';
}
/* '' */
 .sf-bug:before {
   content: '\ue83a';
}
/* '' */
 .sf-calendar_add:before {
   content: '\ue83b';
}
/* '' */
 .sf-calendar_alt:before {
   content: '\ue83c';
}
/* '' */
 .sf-calendar_dates:before {
   content: '\ue83d';
}
/* '' */
 .sf-calendar_remove:before {
   content: '\ue83e';
}
/* '' */
 .sf-calendar_sucsess:before {
   content: '\ue83f';
}
/* '' */
 .sf-calendar:before {
   content: '\ue840';
}
/* '' */
 .sf-call:before {
   content: '\ue841';
}
/* '' */
 .sf-card:before {
   content: '\ue842';
}
/* '' */
 .sf-cart:before {
   content: '\ue843';
}
/* '' */
 .sf-case:before {
   content: '\ue844';
}
/* '' */
 .sf-cat:before {
   content: '\ue845';
}
/* '' */
 .sf-center:before {
   content: '\ue846';
}
/* '' */
 .sf-chair:before {
   content: '\ue847';
}
/* '' */
 .sf-chat:before {
   content: '\ue848';
}
/* '' */
 .sf-check:before {
   content: '\ue849';
}
/* '' */
 .sf-chest_of_drawers:before {
   content: '\ue84a';
}
/* '' */
 .sf-chevron_down:before {
   content: '\ue84b';
}
/* '' */
 .sf-chevron_left:before {
   content: '\ue84c';
}
/* '' */
 .sf-chevron_right:before {
   content: '\ue84d';
}
/* '' */
 .sf-chevron_up:before {
   content: '\ue84e';
}
/* '' */
 .sf-clear_formatting:before {
   content: '\ue84f';
}
/* '' */
 .sf-clipboard_text:before {
   content: '\ue850';
}
/* '' */
 .sf-clipboard:before {
   content: '\ue851';
}
/* '' */
 .sf-clock_alt:before {
   content: '\ue852';
}
/* '' */
 .sf-clock:before {
   content: '\ue853';
}
/* '' */
 .sf-close:before {
   content: '\ue854';
}
/* '' */
 .sf-code_alt:before {
   content: '\ue855';
}
/* '' */
 .sf-code:before {
   content: '\ue856';
}
/* '' */
 .sf-comment_add:before {
   content: '\ue857';
}
/* '' */
 .sf-comment_bubble_alert:before {
   content: '\ue858';
}
/* '' */
 .sf-comment_bubble_question:before {
   content: '\ue859';
}
/* '' */
 .sf-comment_bubble_text:before {
   content: '\ue85a';
}
/* '' */
 .sf-comment_bubble:before {
   content: '\ue85b';
}
/* '' */
 .sf-comment_remove:before {
   content: '\ue85c';
}
/* '' */
 .sf-comment_text:before {
   content: '\ue85d';
}
/* '' */
 .sf-comment:before {
   content: '\ue85e';
}
/* '' */
 .sf-compas:before {
   content: '\ue85f';
}
/* '' */
 .sf-copy:before {
   content: '\ue860';
}
/* '' */
 .sf-cursor_hand:before {
   content: '\ue861';
}
/* '' */
 .sf-cursor:before {
   content: '\ue862';
}
/* '' */
 .sf-cut:before {
   content: '\ue863';
}
/* '' */
 .sf-danger:before {
   content: '\ue864';
}
/* '' */
 .sf-dashboards:before {
   content: '\ue865';
}
/* '' */
 .sf-data:before {
   content: '\ue866';
}
/* '' */
 .sf-delete:before {
   content: '\ue867';
}
/* '' */
 .sf-descrease_indent:before {
   content: '\ue868';
}
/* '' */
 .sf-destroy:before {
   content: '\ue869';
}
/* '' */
 .sf-diagonals_bltr:before {
   content: '\ue86a';
}
/* '' */
 .sf-diagonals_insight:before {
   content: '\ue86b';
}
/* '' */
 .sf-diagonals_tlbr:before {
   content: '\ue86c';
}
/* '' */
 .sf-diagonals:before {
   content: '\ue86d';
}
/* '' */
 .sf-disc:before {
   content: '\ue86e';
}
/* '' */
 .sf-dishwasher:before {
   content: '\ue86f';
}
/* '' */
 .sf-dislike:before {
   content: '\ue870';
}
/* '' */
 .sf-dog:before {
   content: '\ue871';
}
/* '' */
 .sf-download_cloud:before {
   content: '\ue872';
}
/* '' */
 .sf-download:before {
   content: '\ue873';
}
/* '' */
 .sf-draft:before {
   content: '\ue874';
}
/* '' */
 .sf-drop:before {
   content: '\ue875';
}
/* '' */
 .sf-dryer:before {
   content: '\ue876';
}
/* '' */
 .sf-edit:before {
   content: '\ue877';
}
/* '' */
 .sf-eject:before {
   content: '\ue878';
}
/* '' */
 .sf-ellipsis-h_full:before {
   content: '\ue879';
}
/* '' */
 .sf-ellipsis-h:before {
   content: '\ue87a';
}
/* '' */
 .sf-ellipsis-v_full:before {
   content: '\ue87b';
}
/* '' */
 .sf-ellipsis-v:before {
   content: '\ue87c';
}
/* '' */
 .sf-envelope_opened:before {
   content: '\ue87d';
}
/* '' */
 .sf-envelope:before {
   content: '\ue87e';
}
/* '' */
 .sf-error:before {
   content: '\ue87f';
}
/* '' */
 .sf-export:before {
   content: '\ue880';
}
/* '' */
 .sf-external_link:before {
   content: '\ue881';
}
/* '' */
 .sf-eye:before {
   content: '\ue882';
}
/* '' */
 .sf-fast_back:before {
   content: '\ue883';
}
/* '' */
 .sf-fast_forward:before {
   content: '\ue884';
}
/* '' */
 .sf-file:before {
   content: '\ue885';
}
/* '' */
 .sf-filter:before {
   content: '\ue886';
}
/* '' */
 .sf-fin:before {
   content: '\ue887';
}
/* '' */
 .sf-flag:before {
   content: '\ue888';
}
/* '' */
 .sf-folder_closed:before {
   content: '\ue889';
}
/* '' */
 .sf-folder_open_alt:before {
   content: '\ue88a';
}
/* '' */
 .sf-folder_open:before {
   content: '\ue88b';
}
/* '' */
 .sf-left_curved:before {
   content: '\ue88c';
}
/* '' */
 .sf-lightning:before {
   content: '\ue88d';
}
/* '' */
 .sf-like:before {
   content: '\ue88e';
}
/* '' */
 .sf-line:before {
   content: '\ue88f';
}
/* '' */
 .sf-link_alt:before {
   content: '\ue890';
}
/* '' */
 .sf-link:before {
   content: '\ue891';
}
/* '' */
 .sf-list_bullets:before {
   content: '\ue892';
}
/* '' */
 .sf-list_numbered:before {
   content: '\ue893';
}
/* '' */
 .sf-location:before {
   content: '\ue894';
}
/* '' */
 .sf-lock:before {
   content: '\ue895';
}
/* '' */
 .sf-locker:before {
   content: '\ue896';
}
/* '' */
 .sf-loft:before {
   content: '\ue897';
}
/* '' */
 .sf-login:before {
   content: '\ue898';
}
/* '' */
 .sf-logout:before {
   content: '\ue899';
}
/* '' */
 .sf-mac:before {
   content: '\ue89a';
}
/* '' */
 .sf-macbook_and_iphone:before {
   content: '\ue89b';
}
/* '' */
 .sf-macbook:before {
   content: '\ue89c';
}
/* '' */
 .sf-magazine:before {
   content: '\ue89d';
}
/* '' */
 .sf-magnet:before {
   content: '\ue89e';
}
/* '' */
 .sf-map:before {
   content: '\ue89f';
}
/* '' */
 .sf-marker:before {
   content: '\ue8a0';
}
/* '' */
 .sf-math:before {
   content: '\ue8a1';
}
/* '' */
 .sf-mention:before {
   content: '\ue8a2';
}
/* '' */
 .sf-mice_alt:before {
   content: '\ue8a3';
}
/* '' */
 .sf-mice:before {
   content: '\ue8a4';
}
/* '' */
 .sf-minus:before {
   content: '\ue8a5';
}
/* '' */
 .sf-mouse:before {
   content: '\ue8a6';
}
/* '' */
 .sf-music:before {
   content: '\ue8a7';
}
/* '' */
 .sf-mute:before {
   content: '\ue8a8';
}
/* '' */
 .sf-options:before {
   content: '\ue8a9';
}
/* '' */
 .sf-panorama:before {
   content: '\ue8aa';
}
/* '' */
 .sf-pause:before {
   content: '\ue8ab';
}
/* '' */
 .sf-pc:before {
   content: '\ue8ac';
}
/* '' */
 .sf-photo:before {
   content: '\ue8ad';
}
/* '' */
 .sf-picture:before {
   content: '\ue8ae';
}
/* '' */
 .sf-pie:before {
   content: '\ue8af';
}
/* '' */
 .sf-pies:before {
   content: '\ue8b0';
}
/* '' */
 .sf-pin_add:before {
   content: '\ue8b1';
}
/* '' */
 .sf-pin_location_alt:before {
   content: '\ue8b2';
}
/* '' */
 .sf-pin_location:before {
   content: '\ue8b3';
}
/* '' */
 .sf-pin:before {
   content: '\ue8b4';
}
/* '' */
 .sf-plate:before {
   content: '\ue8b5';
}
/* '' */
 .sf-play_skip_back:before {
   content: '\ue8b6';
}
/* '' */
 .sf-play_skip_forward:before {
   content: '\ue8b7';
}
/* '' */
 .sf-play:before {
   content: '\ue8b8';
}
/* '' */
 .sf-plug:before {
   content: '\ue8b9';
}
/* '' */
 .sf-plugin:before {
   content: '\ue8ba';
}
/* '' */
 .sf-plus:before {
   content: '\ue8bb';
}
/* '' */
 .sf-pool:before {
   content: '\ue8bc';
}
/* '' */
 .sf-print:before {
   content: '\ue8bd';
}
/* '' */
 .sf-projector:before {
   content: '\ue8be';
}
/* '' */
 .sf-question_mark:before {
   content: '\ue8bf';
}
/* '' */
 .sf-refresh:before {
   content: '\ue8c0';
}
/* '' */
 .sf-remove_alarm:before {
   content: '\ue8c1';
}
/* '' */
 .sf-remove:before {
   content: '\ue8c2';
}
/* '' */
 .sf-right_align:before {
   content: '\ue8c3';
}
/* '' */
 .sf-right_curved:before {
   content: '\ue8c4';
}
/* '' */
 .sf-round:before {
   content: '\ue8c5';
}
/* '' */
 .sf-router:before {
   content: '\ue8c6';
}
/* '' */
 .sf-sandglass:before {
   content: '\ue8c7';
}
/* '' */
 .sf-save:before {
   content: '\ue8c8';
}
/* '' */
 .sf-scan:before {
   content: '\ue8c9';
}
/* '' */
 .sf-search:before {
   content: '\ue8ca';
}
/* '' */
 .sf-send:before {
   content: '\ue8cb';
}
/* '' */
 .sf-server:before {
   content: '\ue8cc';
}
/* '' */
 .sf-settings_alt:before {
   content: '\ue8cd';
}
/* '' */
 .sf-settings:before {
   content: '\ue8ce';
}
/* '' */
 .sf-share_alt:before {
   content: '\ue8cf';
}
/* '' */
 .sf-share:before {
   content: '\ue8d0';
}
/* '' */
 .sf-shield_secured:before {
   content: '\ue8d1';
}
/* '' */
 .sf-shield:before {
   content: '\ue8d2';
}
/* '' */
 .sf-smile:before {
   content: '\ue8d3';
}
/* '' */
 .sf-smoke:before {
   content: '\ue8d4';
}
/* '' */
 .sf-sound_wave:before {
   content: '\ue8d5';
}
/* '' */
 .sf-speaker:before {
   content: '\ue8d6';
}
/* '' */
 .sf-star:before {
   content: '\ue8d7';
}
/* '' */
 .sf-sticker:before {
   content: '\ue8d8';
}
/* '' */
 .sf-stickers:before {
   content: '\ue8d9';
}
/* '' */
 .sf-stop:before {
   content: '\ue8da';
}
/* '' */
 .sf-stopwatch_timer:before {
   content: '\ue8db';
}
/* '' */
 .sf-stopwatch:before {
   content: '\ue8dc';
}
/* '' */
 .sf-strikethrough:before {
   content: '\ue8dd';
}
/* '' */
 .sf-sun-1:before {
   content: '\ue8de';
}
/* '' */
 .sf-table:before {
   content: '\ue8df';
}
/* '' */
 .sf-tag:before {
   content: '\ue8e0';
}
/* '' */
 .sf-telecom:before {
   content: '\ue8e1';
}
/* '' */
 .sf-template:before {
   content: '\ue8e2';
}
/* '' */
 .sf-text:before {
   content: '\ue8e3';
}
/* '' */
 .sf-tool_alt:before {
   content: '\ue8e4';
}
/* '' */
 .sf-tool:before {
   content: '\ue8e5';
}
/* '' */
 .sf-top_left:before {
   content: '\ue8e6';
}
/* '' */
 .sf-top_right:before {
   content: '\ue8e7';
}
/* '' */
 .sf-tuner_alt:before {
   content: '\ue8e8';
}
/* '' */
 .sf-tuner:before {
   content: '\ue8e9';
}
/* '' */
 .sf-turn_off:before {
   content: '\ue8ea';
}
/* '' */
 .sf-tv_box:before {
   content: '\ue8eb';
}
/* '' */
 .sf-tv:before {
   content: '\ue8ec';
}
/* '' */
 .sf-umbrella:before {
   content: '\ue8ed';
}
/* '' */
 .sf-underline:before {
   content: '\ue8ee';
}
/* '' */
 .sf-unlock:before {
   content: '\ue8ef';
}
/* '' */
 .sf-update:before {
   content: '\ue8f0';
}
/* '' */
 .sf-upload:before {
   content: '\ue8f1';
}
/* '' */
 .sf-user:before {
   content: '\ue8f2';
}
/* '' */
 .sf-verified_face:before {
   content: '\ue8f3';
}
/* '' */
 .sf-verified:before {
   content: '\ue8f4';
}
/* '' */
 .sf-video_tape:before {
   content: '\ue8f5';
}
/* '' */
 .sf-video:before {
   content: '\ue8f6';
}
/* '' */
 .sf-volume:before {
   content: '\ue8f7';
}
/* '' */
 .sf-wallet:before {
   content: '\ue8f8';
}
/* '' */
 .sf-washer:before {
   content: '\ue8f9';
}
/* '' */
 .sf-watch:before {
   content: '\ue8fa';
}
/* '' */
 .sf-webcam:before {
   content: '\ue8fb';
}
/* '' */
 .sf-wi-fi:before {
   content: '\ue8fc';
}
/* '' */
 .sf-window:before {
   content: '\ue8fd';
}
/* '' */
 .sf-wireless:before {
   content: '\ue8fe';
}
/* '' */
 .sf-folder_zip:before {
   content: '\ue8ff';
}
/* '' */
 .sf-frame:before {
   content: '\ue900';
}
/* '' */
 .sf-gamepad:before {
   content: '\ue901';
}
/* '' */
 .sf-glasses:before {
   content: '\ue902';
}
/* '' */
 .sf-globe:before {
   content: '\ue903';
}
/* '' */
 .sf-grid:before {
   content: '\ue904';
}
/* '' */
 .sf-hanger:before {
   content: '\ue905';
}
/* '' */
 .sf-headphones:before {
   content: '\ue906';
}
/* '' */
 .sf-heart:before {
   content: '\ue907';
}
/* '' */
 .sf-help:before {
   content: '\ue908';
}
/* '' */
 .sf-home:before {
   content: '\ue909';
}
/* '' */
 .sf-homev2:before {
   content: '\ue90a';
}
/* '' */
 .sf-horn:before {
   content: '\ue90b';
}
/* '' */
 .sf-ice_cream:before {
   content: '\ue90c';
}
/* '' */
 .sf-idea:before {
   content: '\ue90d';
}
/* '' */
 .sf-import:before {
   content: '\ue90e';
}
/* '' */
 .sf-incert_line:before {
   content: '\ue90f';
}
/* '' */
 .sf-increase_indent:before {
   content: '\ue910';
}
/* '' */
 .sf-info:before {
   content: '\ue911';
}
/* '' */
 .sf-iphone:before {
   content: '\ue912';
}
/* '' */
 .sf-italic:before {
   content: '\ue913';
}
/* '' */
 .sf-key:before {
   content: '\ue914';
}
/* '' */
 .sf-keyboard:before {
   content: '\ue915';
}
/* '' */
 .sf-lamp:before {
   content: '\ue916';
}
/* '' */
 .sf-left_align:before {
   content: '\ue917';
}
/* '' */
 .sf-chevron_right_smpl:before {
   content: '\ue918';
}
/* '' */
 .sf-chevron_left_smpl:before {
   content: '\ue919';
}
/* '' */
 .sf-obo_logo:before {
   content: '\ue91a';
}
/* '' */
 .sf-heart_fill:before {
   content: '\ue91b';
}
/* '' */
 .sf-star_fill:before {
   content: '\ue91c';
}
/* '' */
 .sf-eye_close:before {
   content: '\ue91d';
}
/* '' */
 .sf-food:before {
   content: '\ue91e';
}
/* '' */
 .sf-shop:before {
   content: '\ue91f';
}
/* '' */
 .sf-routerv2:before {
   content: '\ue920';
}
/* '' */
 .sf-wifi:before {
   content: '\ue921';
}
/* '' */
 .sf-wifi_off:before {
   content: '\ue922';
}
/* '' */
 .sf-cpu:before {
   content: '\ue923';
}
/* '' */
 .sf-boxes:before {
   content: '\ue924';
}
/* '' */
 .sf-diagram_2:before {
   content: '\ue925';
}
/* '' */
 .sf-diagram_3:before {
   content: '\ue926';
}
/* '' */
 .sf-parking:before {
   content: '\ue927';
}
/* '' */
`