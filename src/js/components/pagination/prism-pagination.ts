import { LitElement, html } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { bsStyles } from './pagination.css';

@customElement('prism-pagination')
export class PrismPagination extends LitElement {
  @property({type: Number})
  length: number = 6;
  @property({type: Number})
  position: number = 1;
  @property({type: Number})
  shown: number = 3;

  static override styles = bsStyles

  override render() {
    console.log('--render--', this.position)

    let separator: string = '...' 
    let value: Array<Object> = []
    let start_pos: number = this.position
    let end_pos: number = 0
    let mishown = Math.floor(this.shown / 2)

    //Set first page 
    value.push(html`
      <li class="page-item ${this.position == 1 ? 'active':''}" @click=${(_e: Event) => this.setPosition(1, _e)}><a class="page-link" href="#">1</a></li>
    `)

    //Get loop start & end position
    if(this.position >= this.length)
    {
      console.log('completed')
      this.position === this.length
      end_pos = this.length - 1
      start_pos = this.length - this.shown + 1
    }
    else if(this.position > this.length - this.shown)
    {
      console.log('end')
      end_pos = this.length - 1
      start_pos = this.position - mishown > this.length - this.shown ? this.length - this.shown + 1: this.position - mishown
    }
    else if(this.position >= this.shown)
    {
      console.log('mid')
      start_pos = this.position - mishown
      end_pos = this.position + mishown
    }
    else if(this.position > 1)
    {
      console.log('beg')
      start_pos = 2
      end_pos = this.position + mishown
      end_pos <= this.shown ? end_pos = this.shown : ''
    }
    else
    {
      start_pos = 2
      end_pos = this.position + this.shown - 1
    }
    //Fix min start pos
    start_pos <= 1 ? start_pos = 2 : ''

    //Set start separator
    if(start_pos > 3)
    {
      value.push(html`
        <li class="page-item disabled"><span class="page-link">${separator}</span></li>
      `)
    }
    else if(start_pos === 3)
    {
      value.push(html`
        <li class="page-item" @click=${(_e: Event) => this.setPosition(2, _e)}><a class="page-link" href="#">2</a></li>
      `)
    }

    // console.log('Current position :', this.position)
    // console.log('start position :', start_pos)
    // console.log('Last position :', end_pos)
    // console.log('Shown :', this.shown)

    // Loop each pages from start to end
    for(let i = start_pos; i <= end_pos; i++){
      value.push(html`
        <li class="page-item ${this.position == i ? 'active':''}" @click=${(_e: Event) => this.setPosition(i, _e)}><a class="page-link" href="#">${i}</a></li>
      `)
    }

    //Set end separator
    if(end_pos <= this.length - 3)
    {
      value.push(html`
        <li class="page-item disabled"><a class="page-link" href="#">${separator}</a></li>
      `)
    }
    else if(end_pos === this.length - this.shown + 1)
    {
      value.push(html`
        <li class="page-item" @click=${(_e: Event) => this.setPosition(this.length - 1, _e)}><a class="page-link" href="#">${this.length - 1}</a></li>
      `) 
    }

    //Set the last page button
    value.push(html`
      <li class="page-item ${this.position == this.length ? 'active':''}" @click=${(_e: Event) => this.setPosition(this.length, _e)}><a class="page-link" href="#">${this.length}</a></li>
    `)
    

    return html`
    <nav>
      <ul class="pagination">
        <li class="page-item ${this.position == 1 ? 'disabled':''}" @click=${(_e: Event) => this.setPosition(this.position - 1, _e)}><a class="page-link" href="#"><</a></li>
        ${value}
        <li class="page-item ${this.position == this.length ? 'disabled':''}" @click=${(_e: Event) => this.setPosition(this.position + 1, _e)}><a class="page-link" href="#">></a></li>
      </ul>
    </nav>`
  }

  override connectedCallback()
  {
    super.connectedCallback()
    //Prevent showing more page than current page length
    if(this.shown > this.length)
    {
      this.shown = this.length
    }
  }

  private setPosition(i: number, e: Event)
  {
    e.preventDefault()
    i = (i < 1) ? 1 : i
    i = (i > this.length) ? this.length : i
    this.position = i
    this.dispatchEvent(new CustomEvent("setPage", { detail: i }))
  }
}
