import { css } from 'lit';
export const bsStyles = css`
.pagination {
  --obo-pagination-padding-x: 0.75rem;
  --obo-pagination-padding-y: 0.375rem;
  --obo-pagination-font-size: var(--bs-body-font-size);
  --obo-pagination-color: var(--bs-link-color);
  --obo-pagination-bg: var(--bs-body-bg);
  --obo-pagination-border-width: var(--bs-border-width);
  --obo-pagination-border-color: var(--bs-border-color);
  --obo-pagination-border-radius: var(--bs-border-radius);
  --obo-pagination-hover-color: var(--bs-link-hover-color);
  --obo-pagination-hover-bg: var(--bs-tertiary-bg);
  --obo-pagination-hover-border-color: var(--bs-border-color);
  --obo-pagination-focus-color: var(--bs-link-hover-color);
  --obo-pagination-focus-bg: var(--bs-body-bg);
  --obo-pagination-active-color: #fff;
  --obo-pagination-active-bg: var(--bs-primary);
  --obo-pagination-active-border-color: var(--bs-primary);
  --obo-pagination-disabled-color: var(--bs-secondary-color);
  --obo-pagination-disabled-bg: var(--bs-secondary-bg);
  --obo-pagination-disabled-border-color: var(--bs-border-color);
  display: flex;
  padding-left: 0;
  margin:0;
  list-style: none;
}
.page-link {
  position: relative;
  display: block;
  padding: var(--obo-pagination-padding-y) var(--obo-pagination-padding-x);
  font-size: var(--obo-pagination-font-size);
  color: var(--obo-pagination-color);
  text-decoration: none;
  background-color: var(--obo-pagination-bg);
  border: var(--obo-pagination-border-width) solid var(--obo-pagination-border-color);
  transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out;
  user-select: none;
}
@media (prefers-reduced-motion: reduce) {
  .page-link {
    transition: none;
  }
}
.page-link:hover {
  z-index: 2;
  color: var(--obo-pagination-hover-color);
  background-color: var(--obo-pagination-hover-bg);
  border-color: var(--obo-pagination-hover-border-color);
}
.page-link:focus {
  z-index: 3;
  color: var(--obo-pagination-focus-color);
  background-color: var(--obo-pagination-focus-bg);
  outline: 0;
}
.page-link.active, .active > .page-link {
  z-index: 3;
  color: var(--obo-pagination-active-color);
  background-color: var(--obo-pagination-active-bg);
  border-color: var(--obo-pagination-active-border-color);
}
.page-link.disabled, .disabled > .page-link {
  color: var(--obo-pagination-disabled-color);
  pointer-events: none;
  background-color: var(--obo-pagination-disabled-bg);
  border-color: var(--obo-pagination-disabled-border-color);
}
.page-item:not(:first-child) .page-link {
  margin-left: calc(var(--bs-border-width) * -1);
}
.page-item:first-child .page-link {
  border-top-left-radius: var(--obo-pagination-border-radius);
  border-bottom-left-radius: var(--obo-pagination-border-radius);
}
.page-item:last-child .page-link {
  border-top-right-radius: var(--obo-pagination-border-radius);
  border-bottom-right-radius: var(--obo-pagination-border-radius);
}
.pagination-lg {
  --obo-pagination-padding-x: 1.5rem;
  --obo-pagination-padding-y: 0.75rem;
  --obo-pagination-font-size: 1.25rem;
  --obo-pagination-border-radius: var(--bs-border-radius-lg);
}
.pagination-sm {
  --obo-pagination-padding-x: 0.5rem;
  --obo-pagination-padding-y: 0.25rem;
  --obo-pagination-font-size: 0.875rem;
  --obo-pagination-border-radius: var(--bs-border-radius-sm);
}
`